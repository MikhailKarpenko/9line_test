<?php $title = 'Главная'; ?>

<!DOCTYPE html>

<html lang="ru">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1">
    <title><?= $title ?></title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>

<body class="loaded">

<!-- BEGIN BODY -->

    <div class="main-wrapper">

        <!-- BEGIN HEADER -->
        
        <header>
            <div class="wrapper">
        
                <h2>header was here</h2>
        
            </div>
        </header>
        
        <!-- HEADER EOF   -->

        <!-- BEGIN CONTENT -->
        
        <main class="content">
            <div class="wrapper">
                <section class="slick__carousel slider" data-sizes="50vw">
                    <div>
                        <img src="/img/slider/01.jpg" alt="...">
                    </div>
                    <div>
                        <img src="/img/slider/01.jpg" alt="...">
                    </div>
                    <div>
                        <img src="/img/slider/01.jpg" alt="...">
                    </div>
                </section>
            </div>
        </main>
        
        <!-- CONTENT EOF   -->

        <!-- BEGIN FOOTER -->
        
        <footer>
            <div class="wrapper">
        
            </div>
        </footer>
        
        <!-- FOOTER EOF   -->

    </div>

<div class="icon-load"></div>

<!-- BODY EOF   -->

<!-- JS -->
<script src="js/main.js"></script>

</body>
</html>