/** *
 * Modules
 */
//= partials/jquery-3.0.0.min.js
//= partials/jquery-migrate-1.4.1.min.js

//= partials/components/jquery.fancybox.js
//= partials/components/jquery.formstyler.js
//= partials/components/slick.js
//= partials/components/jquery.mCustomScrollbar.js


/**
 * My JS
 */
//= partials/custom.js
//= partials/template.js