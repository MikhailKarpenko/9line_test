/*
 * Java Script
 */

$(document).ready(function() {

/**
 * menu > 991px
 */
    $(function () {

        var $body =  $(document.body);

        $('.toggle-nav').on('click', function () {
            $body.toggleClass('open');
            setNavigationHeight();
        });

        function setNavigationHeight() {

            if($body.hasClass('open')) {
                $('.navigation__wrapper').css('height', ($(window).height() - 140));

                $('.togle-link').on('click', function () {
                    $body.removeClass('open');
                });

            } else {
                $('.navigation__wrapper').css('height', 'auto');
            }
        }

        window.onresize = function () {
            clearTimeout($resizeDone);
            var $resizeDone = setTimeout(function () {
                setNavigationHeight();

                if ($body.width() >= 991 && $body.hasClass('open')) {
                    $body.toggleClass('open');
                }

            }, 300);
        };

    });

/*
 * ScrollMagic
 * 6.10.2017
 */

    // Init ScrollMagic
    var controller = new ScrollMagic.Controller();

    if ($(document.body).width() > 768) {
        // Animations
        $('.animate').each(function () {

            //console.log(this);

            //animate up, right, down, left

            var fadeScene = new ScrollMagic.Scene({
                triggerElement: this,
                triggerHook: 0.95
            })
                .setClassToggle(this, 'fade-in')
                .reverse(false)
                //.addIndicators()
                .addTo(controller);
        });

    }

/**
 * Parallax
 * 6.10.2017
 */

$('.letter-image').each(function () {

    var $ofsetX = $(this).data('ofsetx');

    $ofsetX = $ofsetX + "%" || 0;

    var parallaxTl = new TimelineMax();
    parallaxTl
    //.from('.letter__item', 1, {y: '-10%', ease:Power0.easeIn}, 0)
        .from($(this), 1, {x: $ofsetX, ease: Power0.easeIn}, 0);

    var slideParallaxScene = new ScrollMagic.Scene({
        triggerElement: this,
        triggerHook: .5,
        duration: '250%'
    })
        .setTween(parallaxTl)
        //.addIndicators({name: 'letter-d', colorTrigger: 'black', colorStart: 'pink'})
        .addTo(controller);

});
    /**
     * data-$ofsetx and data-$ofsety
     * from .parallax__item
     * for parallax effect
     *
     * Karpenko M.V. 17.01.2018
     */

    $('.letter__item').each(function () {

        var $ofsetX = $(this).data('ofsetx');

        $ofsetX = $ofsetX + "%" || 0;

        var parallaxTl1 = new TimelineMax();

        parallaxTl1.from( $(this), 1, {x: $ofsetX, ease: Power0.easeIn}, 0);

        var slideParallaxSceneBG = new ScrollMagic.Scene({
            triggerElement: this,
            triggerHook: .3,
            duration: '100%'
        })
            .setTween(parallaxTl1)
            //.addIndicators()
            .addTo(controller);


    });


    /**
     * data-$ofsetx and data-$ofsety
     * from .parallax__item
     * for parallax effect
     *
     * Karpenko M.V. 17.01.2018
     */
    $('.parallax__item').each(function () {

        var $ofsetX = $(this).data('ofsetx');
        var $ofsetY = $(this).data('ofsety');

        $ofsetX = $ofsetX + "%" || 0;
        $ofsetY = $ofsetY + "%" || 0;

        var textParallaxScene = new ScrollMagic.Scene({
            triggerElement: this,
            triggerHook: 1,
            duration: '100%'
        })
            .setTween(this,
            1,
            {
                x: $ofsetX,
                y: $ofsetY,
                ease: Power0.easeIn
            },
            0)
            //.addIndicators()
            .addTo(controller);
    });

});

