<?php $title = 'Главная'; ?>

//= template/_head.php

<body class="loaded">

<!-- BEGIN BODY -->

    <div class="main-wrapper">

        //= template/_header.php

        //= template/_content.php

        //= template/_footer.php

    </div>

<div class="icon-load"></div>

<!-- BODY EOF   -->

<!-- JS -->
<script src="js/main.js"></script>

</body>
</html>


